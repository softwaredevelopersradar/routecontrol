﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

using WpfControlRouteMap.Enums;

using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Xml;
//using AzimutTask.Models;
//using AzimutTask.Editors;
//using AzimutTask.Enums;


// Pol
//using System.Collections.ObjectModel; // forObservableCollection
//using System.Collections.Specialized; // for ObservableCollection

namespace WpfControlRouteMap
{
    [CategoryOrder(CategoryRoute, 1)]
    public class ClassPropertyRout : INotifyPropertyChanged
    {
        #region Constructor
        // Конструктор *******************************************************************
        public ClassPropertyRout()
        {

        }
        // ******************************************************************* Конструктор
        #endregion

        #region Public
        // Public ************************************************************************
        public const string CategoryRoute = "Route";
        #endregion
        // ************************************************************************ Public

        #region INotifyPropertyChanged
        // INotifyPropertyChanged ********************************************************
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        // ******************************************************** INotifyPropertyChanged
        #endregion

        #region Private
        // Private ***********************************************************************
        private int numbPoints = 0;
        private string nameRoute = "";
        private double sRoute=0;

        // LNG
        private string pathTranslation = System.IO.Directory.GetCurrentDirectory() + "\\Languages\\TranslationAzimutTask.xml";
        Languages language = Languages.Eng;


        // *********************************************************************** Private
        #endregion

        #region Properties
        // Properties ********************************************************************

        // -------------------------------------------------------------------------------
        // Name of route

        [DataMember]
        [Category(CategoryRoute)]
        [Browsable(true)]

        public string NameRoute
        {
            get => nameRoute;
            set
            {
                if (nameRoute == value) return;
                nameRoute = value;
                OnPropertyChanged();
            }
        }
        // -------------------------------------------------------------------------------
        // Кол-во точек маршрута

        [DataMember]
        [Category(CategoryRoute)]
        [Browsable(true)]

        public int NumbPoints
        {
            get => numbPoints;
            set
            {
                if (numbPoints == value) return;
                numbPoints = value;
                OnPropertyChanged();
            }
        }
        // -------------------------------------------------------------------------------
        // Длина маршрута

        [DataMember]
        [Category(CategoryRoute)]
        [Browsable(true)]

        public double SRoute
        {
            get => sRoute;
            set
            {
                if (sRoute == value) return;
                sRoute = value;
                OnPropertyChanged();
            }
        }
        // -------------------------------------------------------------------------------

        // ******************************************************************** Properties
        #endregion

        #region Methods
        // Methods ***********************************************************************

        // -------------------------------------------------------------------------------
        public ClassPropertyRout Clone()
        {
            return new ClassPropertyRout
            {
                NameRoute = nameRoute,
                NumbPoints=numbPoints,
                SRoute=sRoute

            };
        }
        // -------------------------------------------------------------------------------
        public void Update(ClassPropertyRout newClassPropertyRout)
        {
            NameRoute = newClassPropertyRout.NameRoute;
            NumbPoints = newClassPropertyRout.NumbPoints;
            SRoute = newClassPropertyRout.SRoute;

        }
        // -------------------------------------------------------------------------------
        public bool Compare(ClassPropertyRout classPropertyRout)
        {
                if (
                    classPropertyRout.SRoute != SRoute ||
                    classPropertyRout.NumbPoints != NumbPoints ||
                    classPropertyRout.NameRoute != NameRoute 
                   )
                    return false;
            return true;
        }
        // -------------------------------------------------------------------------------



        // *********************************************************************** Methods

        #endregion

    } // Class
} // Namespace
