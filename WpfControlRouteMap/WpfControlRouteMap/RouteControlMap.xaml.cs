﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WpfControlRouteMap.Enums;

using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Xml;


namespace WpfControlRouteMap
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class RouteControlMap : UserControl
    {

        private ClassPropertyRout oldPropertyRout;

        // LNG
        private string pathTranslation = System.IO.Directory.GetCurrentDirectory() + "\\Languages\\TranslationRouteTask.xml";
        Languages language = Languages.Rus;


        // But
        // Событие по нажатию кнопки Птичка
        public event EventHandler OnButSaveClick;
        // Конструктор ************************************************************************

        public RouteControlMap()
        {
            InitializeComponent();

            //InitEditor();
            InitComponent();

            // Обработчик в секции Handler
            // Для языка
            PropertyRout.PropertyChanged += PropertyRout_PropertyChanged;

        }
        // ************************************************************************ Конструктор

        #region Init

        private void InitEditor()
        {
            PropertyRoute.Editors.Add(new PropertyRoutEditor(nameof(PropertyRout.NameRoute), PropertyRout.GetType()));
            PropertyRoute.Editors.Add(new PropertyRoutEditor(nameof(PropertyRout.SRoute), PropertyRout.GetType()));
            PropertyRoute.Editors.Add(new PropertyRoutEditor(nameof(PropertyRout.NumbPoints), PropertyRout.GetType()));

        }

        private void InitComponent()
        {
            oldPropertyRout = PropertyRout.Clone();
        }

        #endregion

        #region Handler
        // Handler ****************************************************************************
        // !!! Для языка

        private void PropertyRout_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == nameof(Settings.Language))
            //{
            //    OnLanguageChanged(this, Settings.Language);
            //    ChangeLanguge();
            //}
        }
        // **************************************************************************** Handler
        #endregion

        #region Event
        // Event ******************************************************************************

        public event EventHandler<ClassPropertyRout> OnUpdatePropertyRout = (object sender, ClassPropertyRout eventArgs) => { };
        // ****************************************************************************** Event
        #endregion

        #region Public
        // PropertyRout->Public ***************************************************************
        // "RouteMap" -> смотри файл .xaml

        public ClassPropertyRout PropertyRout
        {
            get
            {
                return (ClassPropertyRout)Resources["RouteMap"];
            }
            set
            {

                if ((ClassPropertyRout)Resources["RouteMap"] == value)
                {
                    oldPropertyRout = value.Clone();
                    return;
                }
                ((ClassPropertyRout)Resources["RouteMap"]).Update(value);
                oldPropertyRout = PropertyRout.Clone();
            }
        }
        // *************************************************************** PropertyRout->Public
        #endregion

        #region Button2
        // Button птичка **********************************************************************

        //private void ApplyClick(object sender, RoutedEventArgs e)
        public void ApplyClick(object sender, RoutedEventArgs e)
        {
            // Событие по нажатию кнопки Птичка
            OnButSaveClick?.Invoke(this, new EventArgs());

            if (!PropertyRout.Compare(oldPropertyRout))
            {
                oldPropertyRout = PropertyRout.Clone();
                OnUpdatePropertyRout(this, PropertyRout);
            }

        }
        // ********************************************************************** Button птичка

        // Button крест ***********************************************************************

        private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            if (!PropertyRout.Compare(oldPropertyRout))
                PropertyRout.Update(oldPropertyRout);

        }
        // *********************************************************************** Button крест
        #endregion

        #region LANGUAGE
        // LNG >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        // ..............................................................................
        public void ChangeLanguge(Languages language)
        {
            this.language = language;
            var translation = LoadDictionary(language);
            if (translation != null)
            {
                UpdateNameProperty(translation);
                UpdateNameCategory(translation);
            }
            SetResourcelanguage(language);
        }
        // ..............................................................................

        public Dictionary<string, string> LoadDictionary(Languages lang)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists(pathTranslation))
                xDoc.Load(pathTranslation);
            else
            {
                switch (lang)
                {

                    case Languages.Rus:
                        // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!");
                        break;
                    case Languages.Eng:
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!");
                        break;
                    case Languages.Az:
                        //MessageBox.Show("File XMLTranslation.xml not found!", "Error!");
                        break;
                    default:
                        // MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!");
                        break;
                }

                return null;
            }
            Dictionary<string, string> TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == lang.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
            return TranslateDic;
        }
        // ..............................................................................
        public void UpdateNameProperty(Dictionary<string, string> TranslateDic)
        {
            
        try
          {
            foreach (var property in PropertyRoute.Properties)
             {
               if (TranslateDic.ContainsKey(property.Name))
                 PropertyRoute.Properties.First(t => t.Name == property.Name).DisplayName = TranslateDic[property.Name];
             }
          }
        catch (Exception)
          {

           }
            
        }


        private void UpdateNameCategory(Dictionary<string, string> TranslateDic)
        {
            try
            {
                foreach (var category in PropertyRoute.Categories)
                {
                    if (TranslateDic.ContainsKey(category.Name))
                        PropertyRoute.Categories.First(t => t.Name == category.Name).HeaderCategoryName = TranslateDic[category.Name];
                }
            }
            catch (Exception)
            {

            }
        }

        // ..............................................................................
        public void SetResourcelanguage(Languages lang)
        {
          
                        //ResourceDictionary dict = new ResourceDictionary();
                        //try
                        //{
                        //    switch (lang)
                        //    {
                        //        case Languages.Eng:
                        //            dict.Source = new Uri(pathResourceEng, UriKind.Relative);
                        //            break;
                        //        case Languages.Rus:
                        //            dict.Source = new Uri(pathResourceRus, UriKind.Relative);
                        //            break;
                        //        default:
                        //            dict.Source = new Uri(pathResourceRus, UriKind.Relative);
                        //            break;
                        //    }

                        //    this.Resources.MergedDictionaries.Add(dict);
                        //}
                        //catch (Exception ex)
                        //{ }
           
        }
        // ..............................................................................

        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> LNG
        #endregion


    } // Class
} // Namespace
