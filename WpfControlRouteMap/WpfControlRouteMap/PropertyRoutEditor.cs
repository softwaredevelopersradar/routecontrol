﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace WpfControlRouteMap
{
    class PropertyRoutEditor : PropertyEditor
    {
        // Dictionary *******************************************************************

        Dictionary<string, string> dictKeyDataTemplate = new Dictionary<string, string>()
        {
            {nameof(ClassPropertyRout.NameRoute),"NameEditor" },
            {nameof(ClassPropertyRout.NumbPoints),"NumbEditor" },
            {nameof(ClassPropertyRout.SRoute),"SEditor" }

        };
        // ******************************************************************* Dictionary

        // Конструктор ******************************************************************

        public PropertyRoutEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;

            var resource = new ResourceDictionary
            {
                Source = new Uri("/ControlSettingsMap;component/Resources/Dictionary.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[PropertyName]];
        }
        // ****************************************************************** Конструктор


    } // Class
} // Namespace
